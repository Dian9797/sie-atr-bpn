import KepegawaianBPNGol from "./kepegawaian-atr-bpn-golongan";
import KepegawaianBPNJK from "./kepegawaian-atr-bpn-JK";
import KepegawaianBPNUsia from "./kepegawaian-atr-bpn-usia";
import KepegawaianBPNPendidikan from "./kepegawaian-atr-bpn-pendidikan";
import KepegawaianBPNJabatan from "./kepegawaian-atr-bpn-jabatan";
import KepegawaianBPNMutasi from "./kepegawaian-atr-bpn-mutasi";
import KepegawaianOrganisasi from "./kepegawaian-organisasi";
import PegawaiAtr from "./PegawaiAtr";
import Organisasi from "./Organisasi";

export {
  KepegawaianBPNGol,
  KepegawaianBPNJK,
  KepegawaianBPNUsia,
  KepegawaianBPNPendidikan,
  KepegawaianBPNJabatan,
  KepegawaianBPNMutasi,
  KepegawaianOrganisasi,
  PegawaiAtr,
  Organisasi,
};
