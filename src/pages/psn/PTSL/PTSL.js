import React from "react";
import SiePsnPtsl5Peringkat from "./sie_psn_ptsl_5peringkat";

const PTSL = () => {
  return (
    <div
      style={{
        marginTop: 20,
        marginBottom: 20,
        width: "100%",
        height: "80vh",
      }}
    >
      <SiePsnPtsl5Peringkat />
    </div>
  );
};

export default PTSL;
